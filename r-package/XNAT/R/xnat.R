library(RCurl)

setClassUnion("characterOrNA", c("character", "logical"))

setGeneric("subjects", function(obj, ...) standardGeneric("subjects"))
setGeneric("project", function(obj, ...) standardGeneric("project"))
setGeneric("experiments", function(obj, ...) standardGeneric("experiments"))
setGeneric("subject", function(obj, ...) standardGeneric("subject"))
setGeneric("scans", function(obj, ...) standardGeneric("scans"))
setGeneric("experiment", function(obj, ...) standardGeneric("experiment"))

csv.from.string <- function(string)
{

    c <- textConnection(string)
    # as.is will keep strings as strings (not convert to factors)
    # colClasses keeps IDs as strings, even if they all look like integers 
    # and R wants to cast them as such (this can happen for scans)
    csv <- read.csv(c, as.is = TRUE, colClasses=list(ID="character"))
    close(c)

    return(csv)

}

# eof
